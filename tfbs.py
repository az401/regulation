import pandas as pd #type: ignore
from typing import Dict, List
from collections import defaultdict
import grn

tfbs_f = "/Users/argyris/Downloads/TFBS_Floral_Regulators.xlsx"
grn_f = "/Users/argyris/refahi_etal_2020/data/refNet2.txt"


def read_tfbs(fn: str) -> Dict[str, List[str]]:
    intrs = pd.read_excel(fn, header=None)

    d_ = defaultdict(list)
    for s, s_ in zip(intrs[1], intrs[2]):
        src = s.replace('"', '')
        dest = s_.replace('"', '').split(",")
        for d in dest:
            d_[d].append(src)

    return d_


def rename_dictl(d: Dict[str, List[str]],
                 s: str,
                 s_: str) -> Dict[str, List[str]]:
    d_ = defaultdict(list)
    for k, vs in d.items():
        vs_ = list()
        for v in vs:
            if v == s:
                vs_.append(s_)
            else:
                vs_.append(v)

        if k == s:
            nk = s_
        else:
            nk = k

        d_[nk].extend(list(set(vs_)))

    return d_

def rename_dictl_n(d: Dict[str, List[str]],
                   ss: List[str],
                   s_: str) -> Dict[str, List[str]]:
    for s in ss:
        d = rename_dictl(d, s, s_)

    return d

def get_tfbs_intrs(fn: str) -> Dict[str, List[str]]:
    d_ = read_tfbs(tfbs_f)
    d_ = rename_dictl_n(d_, ["AP1", "AP2"], "AP1_2")
    d_ = rename_dictl(d_, "ETT", "ETTIN")
    d_ = rename_dictl_n(d_, ["CUC1", "CUC2", "CUC3"], "CUC1_2_3")
    d_ = rename_dictl_n(d_, ["PHB", "PHV"], "PHB_PHV")

    return d_


def get_grn_intrs(fn: str) -> Dict[str, List[str]]:
    G = grn.GRN(fn)
    d_grn = defaultdict(list)
    for src, dest in G.toPairs():
        d_grn[dest].append(src)

    return d_grn

